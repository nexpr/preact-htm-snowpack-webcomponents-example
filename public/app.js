import htm from "./web_modules/htm.js"
import { h, render } from "./web_modules/preact.js"
import { useState, useCallback, useRef } from "./web_modules/preact/hooks.js"
import { DrawItems } from "./draw-items.js"
import "./draw-preview.js"
import util from "./util.js"

const html = htm.bind(h)

export const App = () => {
	const preview_ref = useRef()

	const [items, setItems] = useState([])

	const download = useCallback(() => {
		const url = preview_ref.current.getDownloadURL()
		util.downloadURL(url, "image.png")
	}, [])

	const preview = useCallback((new_items) => {
		setItems(new_items)
	}, [items])
	
	return html`
		<div class="container">
			<div class="leftside">
				<${DrawItems} commit=${preview} />
			</div>
			<div class="rightside">
				<draw-preview ref=${preview_ref} draw_items=${items}></draw-preview>
				<button onclick=${download}>Download as PNG</button>
			</div>
		</div>
	`
}
