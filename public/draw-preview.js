customElements.define("draw-preview", class extends HTMLElement {
	constructor() {
		super()
		this.attachShadow({mode: "open"}).innerHTML = `
			<style>
				canvas {
					width: 300px;
					height: 300px;
					border: 1px solid #aaa;
				}
			</style>
			<canvas></canvas>
		`
	}

	redraw() {
		const new_canvas = document.createElement("canvas")
		new_canvas.width = 300
		new_canvas.height = 300
		const ctx = new_canvas.getContext("2d")
		ctx.font = "24px Arial, meiryo"

		for(const item of this.items) {
			ctx.fillText(item.text, item.x, item.y)
		}

		this.shadowRoot.querySelector("canvas").replaceWith(new_canvas)
	}

	set draw_items(items) {
		this.items = items
		this.redraw()
	}

	getDownloadURL() {
		return this.shadowRoot.querySelector("canvas").toDataURL()
	}
})
