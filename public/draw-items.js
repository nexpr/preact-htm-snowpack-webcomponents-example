import htm from "./web_modules/htm.js"
import { h, render } from "./web_modules/preact.js"
import { useState, useCallback } from "./web_modules/preact/hooks.js"
import { DrawItem } from "./draw-item.js"

const html = htm.bind(h)

const createNewItem = () => {
	return { text: "", x: 0, y: 0 }
}

export const DrawItems = (props) => {
	const [items, setItems] = useState([createNewItem()])

	const add = useCallback(() => {
		setItems(items.concat([createNewItem()]))
	}, [items])

	const update = useCallback((target_item, name, value) => {
		const new_items = items.map(item => {
			if (item === target_item) return { ...item, [name]: value }
			else return item
		})
		setItems(new_items)
	}, [items])

	const commit = useCallback(() => {
		props.commit(items)
	}, [items])

	const clear = useCallback(() => {
		setItems([])
	}, [items])
	
	return html`
		<div class="header">
			<div class="item">
				<span>text</span>
				<span>x</span>
				<span>y</span>
			</div>
		</div>
		<div class="items">
			${
				items.map(item => html`<${DrawItem} item=${item} oninput=${update} />`)
			}
		</div>
		<div class="buttons">
			<button onclick=${add}>+</button>
			<button onclick=${commit}>Draw</button>
			<button onclick=${clear}>Clear</button>
		</div>
	`
}
