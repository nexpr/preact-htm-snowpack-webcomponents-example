import htm from "./web_modules/htm.js"
import { h, render } from "./web_modules/preact.js"
import { App } from "./app.js"

const html = htm.bind(h)
render(html`<${App} />`, document.getElementById("root"))
