import htm from "./web_modules/htm.js"
import { h, render } from "./web_modules/preact.js"
import { useCallback } from "./web_modules/preact/hooks.js"

const html = htm.bind(h)

export const DrawItem = (props) => {
	const oninput = useCallback((eve) => {
		let { name, value } = eve.target
		if (name) {
			if (name === "x" || name === "y") {
				value = String(+value.replace(/[^0-9]+/g, ""))
			}
			props.oninput(props.item, name, value)
		}
	}, [props])

	return html`
		<div class="item" oninput=${oninput}>
			<input name="text" value=${props.item.text} />
			<input name="x" value=${props.item.x} />
			<input name="y" value=${props.item.y} />
		</div>
	`
}
