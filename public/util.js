export default {
	downloadURL(url, name) {
		const a = document.createElement("a")
		if (name) a.download = name
		a.href = url
		a.click()
	}
}
